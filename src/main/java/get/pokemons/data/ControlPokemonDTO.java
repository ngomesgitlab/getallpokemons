package get.pokemons.data;

import lombok.Data;

import java.util.HashMap;
import java.util.Map;

import static get.pokemons.common.constants.PokemonConstants.POKEMON_NUMBER_EACH_SPOT;

@Data
public class ControlPokemonDTO {

    private int actualHorizontalPosition;
    private int actualVerticalPosition;
    private int numberPokemonFounded;
    private Map<String, Boolean> historyPokemonPositionMap;

    /**
     * You Start this game at position 0,0, because of this :
     * 1 - actualHorizontalPosition = 0;
     * 2 - actualVerticalPosition = 0;
     * 3 - numberPokemonFounded = 1; (Pokemon(s) in 0,0 position)
     * 4 - historyPokemonPositionMap = 00 , true (You have already visited this position)
     */
    public ControlPokemonDTO() {

        this.actualHorizontalPosition = 0;
        this.actualVerticalPosition = 0;
        this.numberPokemonFounded = POKEMON_NUMBER_EACH_SPOT;
        this.historyPokemonPositionMap = new HashMap<>() {  {
                put("00", true);
            }};

    }

}