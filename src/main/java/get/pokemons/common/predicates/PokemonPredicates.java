package get.pokemons.common.predicates;

import get.pokemons.common.functions.PokemonFunctions;
import get.pokemons.data.ControlPokemonDTO;

import java.util.function.Predicate;

@SuppressWarnings({ "java:S1444", "java:S1104" })
public class PokemonPredicates {

    private PokemonPredicates() {
        throw new IllegalStateException("Utility class");
    } //SonarLint Alert

    /**
     * Validate if Actual Vertical Positions + Actual Horizontal Position on HistoryPositionMap
     */
    public static Predicate<ControlPokemonDTO> isANewDirection = controlPokemonDTO ->
        controlPokemonDTO.getHistoryPokemonPositionMap()
            .entrySet()
            .parallelStream()
            .noneMatch(map ->
                map.getKey().equalsIgnoreCase(PokemonFunctions.concatenateHorAndVerBiDimensionalArray.apply(controlPokemonDTO)));

}
