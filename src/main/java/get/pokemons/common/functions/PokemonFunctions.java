package get.pokemons.common.functions;

import get.pokemons.common.predicates.GenericPredicates;
import get.pokemons.common.predicates.PokemonPredicates;
import get.pokemons.data.ControlPokemonDTO;

import java.util.Map;
import java.util.function.Function;
import java.util.function.IntUnaryOperator;
import java.util.function.UnaryOperator;

import static get.pokemons.common.constants.PokemonConstants.POKEMON_NUMBER_EACH_SPOT;
import static get.pokemons.common.constants.PokemonConstants.POKEMON_VALID_DIRECTIONS;

@SuppressWarnings({ "java:S1444", "java:S1104" })
public class PokemonFunctions {

    private PokemonFunctions() {
        throw new IllegalStateException("Utility class");
    } //SonarLint Alert

    /**
     * Concatenated Horizontal and Vertical Array Position.
     * This is the key of historyPokemonPositionMap
     */
    public static Function<ControlPokemonDTO, String> concatenateHorAndVerBiDimensionalArray = controlPokemonDTO ->
        new StringBuffer().append(controlPokemonDTO.getActualHorizontalPosition()).append(controlPokemonDTO.getActualVerticalPosition()).toString();



    /**
     * Just update Number of Pokemon founded
     */
    private static IntUnaryOperator updateNumberOfPokemonFounded = actualPokemonFounded -> actualPokemonFounded + POKEMON_NUMBER_EACH_SPOT;


    /**
     * Process to update Pokemon History Position
     */
    private static Function<ControlPokemonDTO, Map<String, Boolean>> updatePokemonHistoryPosition = controlPokemonDTO -> {

        controlPokemonDTO
            .getHistoryPokemonPositionMap()
            .put(concatenateHorAndVerBiDimensionalArray.apply(controlPokemonDTO), true);

        return controlPokemonDTO.getHistoryPokemonPositionMap();

    };



    /**
     * This method will validate if you moved to a new direction,
     * if yes :
     *      1- Increase number of Pokemon founded
     *      2- Update Pokemon history position
     * if no :
     *      Don´t need to do anything, just return the object
     */
    public static UnaryOperator<ControlPokemonDTO> updateNumberAndHistoryPosition = controlPokemonDTO -> {

        if (PokemonPredicates.isANewDirection.test(controlPokemonDTO)) {

            /**
             * First Update Number of Pokemon Founded
             */
            controlPokemonDTO.setNumberPokemonFounded(
                updateNumberOfPokemonFounded.applyAsInt(controlPokemonDTO.getNumberPokemonFounded()));

            /**
             * Second Update History Positions
             */
            controlPokemonDTO.setHistoryPokemonPositionMap(
                updatePokemonHistoryPosition.apply(controlPokemonDTO));

        }

        return controlPokemonDTO;

    };


    /**
     * The only valid Directions are N, O, E, S
     * Remove unnecessary validations from application
     */
    public static UnaryOperator<String> normalizeDirectionString = directions  -> {

        //Return Empty , Avoiding Null Pointers
        if (GenericPredicates.checkIfNullOrEmpty.test(directions)) return "";

        //Remove Incorrect Entries (Valid Ones are : S,O,N,E)
        return directions.toUpperCase().replaceAll(POKEMON_VALID_DIRECTIONS, "");

    };



}
