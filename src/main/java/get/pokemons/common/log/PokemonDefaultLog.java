package get.pokemons.common.log;


import lombok.extern.slf4j.Slf4j;

import static get.pokemons.common.constants.PokemonConstants.POKEMON_CONGRATULATION_MSG;

@Slf4j
public class PokemonDefaultLog {

    private PokemonDefaultLog() {
        throw new IllegalStateException("Utility class");
    } //SonarLint Alert

    /**
     * Log Default Error Message
     */
    public static void logPokemonErrorMessage(
        String errorMessage,
        String detail,
        Exception ex) {

        log.error(String.format(errorMessage, detail), ex);

    }

    /**
     * Being Generic if we want to change we can change only in one place
     * Like if we want to start all outputs with "Ash Got XXX."
     * Log if we need to validate it after runtime.
     */
    @SuppressWarnings({ "java:S106" })
    public static void logNumberOfPokemon(
        Object numberOfPokemon,
        String directions) {

        log.info(String.format(POKEMON_CONGRATULATION_MSG, numberOfPokemon, directions));
        System.out.println(numberOfPokemon);

    }



}
