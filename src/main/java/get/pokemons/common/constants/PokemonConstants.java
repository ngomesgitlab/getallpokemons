package get.pokemons.common.constants;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class PokemonConstants {

    /**
     * FROM ENV VARIABLES - Default = 1
     */
    public static Integer POKEMON_NUMBER_EACH_SPOT;

    @Value("${pokemon.number.spot}")
    public void setPokemonNumberSpot(Integer pokemonNumberSpot) {
        POKEMON_NUMBER_EACH_SPOT = pokemonNumberSpot;
    }


    /**
     * GAME CONSTANTS
     */
    public static final String POKEMON_VALID_DIRECTIONS = "[^N|^E|^O|^S|]";

    /**
     * DIRECTIONS
     */
    public static final String NORTH = "N";
    public static final String EAST = "E";
    public static final String WEST = "O";
    public static final String SOUTH = "S";

    /**
     * MENU
     */
    public static final String TITLE = "- Pokemon: Let´s Get It On -";
    public static final String BORDER =  "-----------------------------";
    public static final String NEXT_DIRECTION = "Write in one line witch movements you one to go (N - NORTH / E - EAST / S - SOUTH / O - WEST).";
    public static final String POKEMON_CONGRATULATION_MSG = "Congratulation Ash, you got %s Pokemon , follow %s directions.";
    public static final String GAME_INSTRUCTIONS =

        "O Ash está a apanhar pokémons num mundo que consiste numa grelha bidimensional infinita de casas. Em cada casa há exatamente um pokémon.\n" +
            "Ele começa por apanhar o pokémon que está na casa onde começa.\n" +
            "A seguir, move-se para a casa imediatamente a norte, sul, este ou oeste de onde se encontra e apanha o pokémon que aí se encontrar, e assim sucessivamente. \n" +
            "\n" +

            "Observçöes: \n \n" +
            "1 - Digite em uma única linha a sequência de movimentos desejada. Cada movimento é descrito por uma letra N, S, E ou O(respetivamente: norte, sul, este, oeste). \n" +
            "2 - Näo se faz necessário dar espaços entre as direções.\n" +
            "3 - Outros caracteres digitados serão descartados nesta caçada.\n" +
            "4 - Os caracteres digitados nesta caçada näo säo case sensitive.\n" +
            "5 - Se o Ash passar numa casa onde já passou (e, portanto, onde já apanhou um pokémon), já lá não está um pokémon para ele apanhar. \n" +
            "\n" +
            "Boa caçada. Que os jogos comecem.";

    /**
     * Messages
     */
    public static final String ERROR_DURING_GET_POKEMON_PROCESS = "There is an error trying to get Pokemon using directions %s.";

}
