package get.pokemons;

import get.pokemons.common.predicates.GenericPredicates;
import get.pokemons.mainmenu.IMainMenu;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PokemonApplication implements CommandLineRunner {

    @Autowired
    private IMainMenu mainMenu;

    public static void main(String[] args) {
        SpringApplication.run(PokemonApplication.class, args);
    }

    @Override
    public void run(String... args) {

        if (GenericPredicates.checkIfNullOrEmpty.negate().test(args) && args[0].equalsIgnoreCase("startApplication")) {
            mainMenu.mainMenu();
        }

    }

}
