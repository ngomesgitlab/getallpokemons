package get.pokemons.services;

import get.pokemons.common.functions.PokemonFunctions;
import get.pokemons.common.log.PokemonDefaultLog;
import get.pokemons.services.interfaces.IGetPokemonServices;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import static get.pokemons.common.constants.PokemonConstants.ERROR_DURING_GET_POKEMON_PROCESS;

@Service
@Slf4j
@RequiredArgsConstructor
public class GetPokemonServices implements IGetPokemonServices {

    private final GetPokemonControlProcess getPokemonControlProcess;

    @Override
    public int getPokemonServices(String directions) {

        int numberPokemonFounded;

        /**
         * Standard Error Message if there is any problem during execution
         */
        try {

            /**
             * Normalize Directions String
             */
            directions = PokemonFunctions.normalizeDirectionString.apply(directions);

            /**
             * Process Get Pokemon
             */
            numberPokemonFounded = getPokemonControlProcess.executeGetPokemonServices(directions);


        } catch (Exception ex) {

            /**
             * In case of an error.
             * Returning and Logging 0 pokemon catch
             */
            numberPokemonFounded = 0;
            PokemonDefaultLog.logPokemonErrorMessage(ERROR_DURING_GET_POKEMON_PROCESS, directions, ex);

        }

        return numberPokemonFounded;

    }


}
