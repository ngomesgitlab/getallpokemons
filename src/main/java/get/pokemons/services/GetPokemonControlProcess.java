package get.pokemons.services;

import get.pokemons.data.ControlPokemonDTO;
import get.pokemons.services.manager.IGetPokemonValidationHandles;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;


@Slf4j
@Service
@RequiredArgsConstructor
public class GetPokemonControlProcess {

    private final GetPokemonControlHandles getPokemonControlHandles;

    protected int executeGetPokemonServices(String directions) {

        ControlPokemonDTO controlPokemonDTO = new ControlPokemonDTO();

        /**
         * Iterate Each Letter of Direction Received
         */
        for (int position = 0; position < directions.length(); position++) {

            /**
             * Iterate by Handle
             */
            for (IGetPokemonValidationHandles getPokemonValidationHandle : getPokemonControlHandles.getGetPokemonValidationHandlesMap().handlesList()) {

                /**
                 * If there is a Handle we will execute the process
                 */
                if (getPokemonValidationHandle.canHandle(directions.substring(position, position + 1))) {

                    /**
                     * Move to new Direction
                     */
                    controlPokemonDTO = getPokemonValidationHandle.getPokemonNewPosition(controlPokemonDTO);

                    /**
                     * Update (If Necessary) PokemonNumbers and Direction
                     */
                    controlPokemonDTO = getPokemonValidationHandle.validateAndUpdateControlPokemonDTO(controlPokemonDTO);

                    /**
                     * Break, only one handle can validate an entry
                     */
                    break;

                }

            }

        }

        return controlPokemonDTO.getNumberPokemonFounded();

    }
}
