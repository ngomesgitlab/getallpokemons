package get.pokemons.services.manager;


import get.pokemons.data.ControlPokemonDTO;

public interface IGetPokemonValidationHandles {

    boolean canHandle(String direction);

    ControlPokemonDTO getPokemonNewPosition(ControlPokemonDTO controlPokemonDTO);

    ControlPokemonDTO validateAndUpdateControlPokemonDTO(ControlPokemonDTO controlPokemonDTO);

}
