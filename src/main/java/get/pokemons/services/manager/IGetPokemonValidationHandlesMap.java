package get.pokemons.services.manager;

import java.util.List;

public interface IGetPokemonValidationHandlesMap<H> {

    List<H> handlesList();

    void addHandle(H handle);

}
