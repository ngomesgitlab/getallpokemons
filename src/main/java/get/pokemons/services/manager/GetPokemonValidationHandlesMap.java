package get.pokemons.services.manager;

import java.util.ArrayList;
import java.util.List;

public class GetPokemonValidationHandlesMap implements IGetPokemonValidationHandlesMap<IGetPokemonValidationHandles> {

    private final ArrayList<IGetPokemonValidationHandles> handlesList;

    public GetPokemonValidationHandlesMap() {
        handlesList = new ArrayList<>();
    }


    @Override
    public List<IGetPokemonValidationHandles> handlesList() {
        return handlesList;
    }

    @Override
    public void addHandle(IGetPokemonValidationHandles handle) {
        if (handle != null)
            handlesList.add(handle);
    }

}
