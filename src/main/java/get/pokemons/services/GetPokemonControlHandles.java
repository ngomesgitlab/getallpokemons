
package get.pokemons.services;

import get.pokemons.services.handles.GoEastHandle;
import get.pokemons.services.handles.GoNorthHandle;
import get.pokemons.services.handles.GoSouthHandle;
import get.pokemons.services.handles.GoWestHandle;
import get.pokemons.services.manager.GetPokemonValidationHandlesMap;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Slf4j
@Service
@RequiredArgsConstructor
@Getter
public class GetPokemonControlHandles {

    private GetPokemonValidationHandlesMap getPokemonValidationHandlesMap;

    private final GoEastHandle goEastHandle;
    private final GoNorthHandle goNorthHandle;
    private final GoSouthHandle goSouthHandle;
    private final GoWestHandle goWestHandle;

    @PostConstruct
    private void startGetPokemonHandles() {
        populateGetPokemonHandles();
    }

    /**
     * Populate Chain of Responsibility Handles
     */
    private void populateGetPokemonHandles() {
        getPokemonValidationHandlesMap = new GetPokemonValidationHandlesMap();
        getPokemonValidationHandlesMap.addHandle(goEastHandle);
        getPokemonValidationHandlesMap.addHandle(goNorthHandle);
        getPokemonValidationHandlesMap.addHandle(goSouthHandle);
        getPokemonValidationHandlesMap.addHandle(goWestHandle);
    }

}
