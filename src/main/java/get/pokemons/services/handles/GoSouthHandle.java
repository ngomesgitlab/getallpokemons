
package get.pokemons.services.handles;

import get.pokemons.common.functions.PokemonFunctions;
import get.pokemons.data.ControlPokemonDTO;
import get.pokemons.services.manager.IGetPokemonValidationHandles;
import org.springframework.stereotype.Service;

import static get.pokemons.common.constants.PokemonConstants.SOUTH;

@Service
public class GoSouthHandle implements IGetPokemonValidationHandles {

    @Override
    public boolean canHandle(String direction) {
        return SOUTH.equalsIgnoreCase(direction);
    }

    @Override
    public ControlPokemonDTO getPokemonNewPosition(ControlPokemonDTO controlPokemonDTO) {
        controlPokemonDTO.setActualVerticalPosition(controlPokemonDTO.getActualVerticalPosition() - 1);
        return controlPokemonDTO;
    }

    @Override
    public ControlPokemonDTO validateAndUpdateControlPokemonDTO(ControlPokemonDTO controlPokemonDTO) {
        return PokemonFunctions.updateNumberAndHistoryPosition.apply(controlPokemonDTO);
    }

}
