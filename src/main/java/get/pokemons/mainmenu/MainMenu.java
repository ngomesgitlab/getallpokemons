package get.pokemons.mainmenu;

import get.pokemons.common.log.PokemonDefaultLog;
import get.pokemons.services.GetPokemonServices;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Scanner;

import static get.pokemons.common.constants.PokemonConstants.BORDER;
import static get.pokemons.common.constants.PokemonConstants.GAME_INSTRUCTIONS;
import static get.pokemons.common.constants.PokemonConstants.NEXT_DIRECTION;
import static get.pokemons.common.constants.PokemonConstants.TITLE;

@Service
@RequiredArgsConstructor
@SuppressWarnings({ "java:S106"})
public class MainMenu implements IMainMenu {

    private final GetPokemonServices getPokemonServices;

    @Override
    public void mainMenu() {

        Scanner s = new Scanner(System.in);

        System.out.println();
        System.out.println(BORDER);
        System.out.println(TITLE);
        System.out.println(BORDER);
        System.out.println();
        System.out.println(GAME_INSTRUCTIONS);
        System.out.println();
        System.out.println(NEXT_DIRECTION);
        String directions = s.next().toUpperCase();
        System.out.println();
        PokemonDefaultLog.logNumberOfPokemon(getPokemonServices.getPokemonServices(directions), directions);
        System.out.println();
        System.out.println();

    }


}
