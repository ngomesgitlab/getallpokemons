package get.pokemons.unittests.common.predicates;

import get.pokemons.PokemonApplication;
import get.pokemons.common.predicates.PokemonPredicates;
import get.pokemons.data.ControlPokemonDTO;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = {PokemonApplication.class})
@ContextConfiguration
class PokemonPredicatesTest {

    @Test
    void should_Return_False_If_Position_Is_Already_In_History_Map() {

        Assertions.assertFalse(PokemonPredicates.isANewDirection.test(new ControlPokemonDTO()));

    }


    @Test
    void should_Return_True_If_You_Move_To_New_North_Position() {

        ControlPokemonDTO controlPokemonDTO = new ControlPokemonDTO();
        controlPokemonDTO.setActualVerticalPosition(1);

        Assertions.assertTrue(PokemonPredicates.isANewDirection.test(controlPokemonDTO));

    }

    @Test
    void should_Return_True_If_You_Move_To_New_South_Position() {

        ControlPokemonDTO controlPokemonDTO = new ControlPokemonDTO();
        controlPokemonDTO.setActualVerticalPosition(-1);

        Assertions.assertTrue(PokemonPredicates.isANewDirection.test(controlPokemonDTO));

    }

    @Test
    void should_Return_True_If_You_Move_To_New_West_Position() {

        ControlPokemonDTO controlPokemonDTO = new ControlPokemonDTO();
        controlPokemonDTO.setActualHorizontalPosition(-1);

        Assertions.assertTrue(PokemonPredicates.isANewDirection.test(controlPokemonDTO));

    }

    @Test
    void should_Return_True_If_You_Move_To_New_East_Position() {

        ControlPokemonDTO controlPokemonDTO = new ControlPokemonDTO();
        controlPokemonDTO.setActualHorizontalPosition(+1);

        Assertions.assertTrue(PokemonPredicates.isANewDirection.test(controlPokemonDTO));

    }


}
