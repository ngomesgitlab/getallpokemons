package get.pokemons.unittests.common.constants;

import get.pokemons.PokemonApplication;
import get.pokemons.common.predicates.GenericPredicates;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static get.pokemons.common.constants.PokemonConstants.EAST;
import static get.pokemons.common.constants.PokemonConstants.NORTH;
import static get.pokemons.common.constants.PokemonConstants.SOUTH;
import static get.pokemons.common.constants.PokemonConstants.WEST;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = {PokemonApplication.class})
@ContextConfiguration
class PokemonConstantsTest {

    @Test
    void should_Be_A_Valid_Constant() {

        Assertions.assertFalse(GenericPredicates.checkIfNullOrEmpty.test(NORTH));
        Assertions.assertFalse(GenericPredicates.checkIfNullOrEmpty.test(EAST));
        Assertions.assertFalse(GenericPredicates.checkIfNullOrEmpty.test(SOUTH));
        Assertions.assertFalse(GenericPredicates.checkIfNullOrEmpty.test(WEST));

    }

}
