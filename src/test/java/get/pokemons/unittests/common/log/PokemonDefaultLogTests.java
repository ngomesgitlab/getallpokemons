package get.pokemons.unittests.common.log;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.spi.ILoggingEvent;
import get.pokemons.PokemonApplication;
import get.pokemons.common.log.PokemonDefaultLog;
import get.pokemons.testsupport.helpers.LogHelpMethods;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;

import static get.pokemons.common.constants.PokemonConstants.ERROR_DURING_GET_POKEMON_PROCESS;
import static get.pokemons.common.constants.PokemonConstants.POKEMON_CONGRATULATION_MSG;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = {PokemonApplication.class})
@ContextConfiguration
class PokemonDefaultLogTests {


    /**
     * logNumberOrMessage
     */

    @Test
    @SuppressWarnings("squid:S2699") //Being Asserted in Another Class
    void shouldLogNumberOfPokemon() {

        String directions = "NEW";

        List<ILoggingEvent> loggingEventList = LogHelpMethods.startGetLoggerMessagesWithLevel.apply(Level.DEBUG);

        PokemonDefaultLog.logNumberOfPokemon(1001, directions);

        LogHelpMethods.loggingEventsContainsString.accept(loggingEventList, 1001);
        LogHelpMethods.loggingEventsContainsString.accept(loggingEventList, POKEMON_CONGRATULATION_MSG.substring(0, 10));
        LogHelpMethods.loggingEventsContainsString.accept(loggingEventList, directions);

    }


    @Test
    @SuppressWarnings("squid:S2699") //Being Asserted in Another Class
    void shouldLogPokemonErrorMessage() {

        String directions = "NEW";

        List<ILoggingEvent> loggingEventList = LogHelpMethods.startGetLoggerMessagesWithLevel.apply(Level.ERROR);

        PokemonDefaultLog.logPokemonErrorMessage(ERROR_DURING_GET_POKEMON_PROCESS, directions, new Exception());

        LogHelpMethods.loggingEventsContainsString.accept(loggingEventList, ERROR_DURING_GET_POKEMON_PROCESS.substring(0, 10));
        LogHelpMethods.loggingEventsContainsString.accept(loggingEventList, directions);

    }


}
