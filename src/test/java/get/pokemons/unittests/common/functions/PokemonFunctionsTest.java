package get.pokemons.unittests.common.functions;

import get.pokemons.PokemonApplication;
import get.pokemons.common.functions.PokemonFunctions;
import get.pokemons.data.ControlPokemonDTO;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static get.pokemons.common.constants.PokemonConstants.POKEMON_NUMBER_EACH_SPOT;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = {PokemonApplication.class})
@ContextConfiguration
class PokemonFunctionsTest {


    /**
     * updateNumberAndHistoryPosition
     */

    /**
     * If updateNumberAndHistoryPosition validates that is not a new position
     * Should return the same object
     *      1- Should not add 1 to numberOfPokemon
     *      2- Should not update historyPokemonPositionMap
     */
    @Test
    void should_Not_Update_Pokemon_And_HistoryMap_If_OldPosition() {

        ControlPokemonDTO controlPokemonDTO = new ControlPokemonDTO();

        ControlPokemonDTO updateControlPokemonDTO = PokemonFunctions.updateNumberAndHistoryPosition.apply(controlPokemonDTO);

        Assertions.assertEquals(0, updateControlPokemonDTO.getActualHorizontalPosition());
        Assertions.assertEquals(0, updateControlPokemonDTO.getActualVerticalPosition());
        Assertions.assertEquals(POKEMON_NUMBER_EACH_SPOT, updateControlPokemonDTO.getNumberPokemonFounded());
        Assertions.assertEquals(1, updateControlPokemonDTO.getHistoryPokemonPositionMap().size());

    }



    /**
     * If updateNumberAndHistoryPosition validates that is a new position
     * Should return the object with
     *      1- numberOfPokemon + 1
     *      2- historyPokemonPositionMap + new position
     */
    @Test
    void should_Update_Pokemon_And_HistoryMap_If_New_Horizontal_Position() {

        //Create and Update ControlPokemonDTO
        ControlPokemonDTO controlPokemonDTO = new ControlPokemonDTO();
        controlPokemonDTO.setActualHorizontalPosition(controlPokemonDTO.getActualHorizontalPosition() + 1);

        controlPokemonDTO = PokemonFunctions.updateNumberAndHistoryPosition.apply(controlPokemonDTO);

        Assertions.assertEquals(1 , controlPokemonDTO.getActualHorizontalPosition());
        Assertions.assertEquals(0, controlPokemonDTO.getActualVerticalPosition());
        Assertions.assertEquals(POKEMON_NUMBER_EACH_SPOT * 2, controlPokemonDTO.getNumberPokemonFounded());
        Assertions.assertEquals(2, controlPokemonDTO.getHistoryPokemonPositionMap().size());

    }


    /**
     * If updateNumberAndHistoryPosition validates that is a new position
     * Should return the object with
     *      1- numberOfPokemon + 1
     *      2- historyPokemonPositionMap + new position
     */
    @Test
    void should_Update_Pokemon_And_HistoryMap_If_New_Vertical_Position() {

        //Create and Update ControlPokemonDTO
        ControlPokemonDTO controlPokemonDTO = new ControlPokemonDTO();
        controlPokemonDTO.setActualHorizontalPosition(controlPokemonDTO.getActualVerticalPosition() + 1);

        controlPokemonDTO = PokemonFunctions.updateNumberAndHistoryPosition.apply(controlPokemonDTO);

        Assertions.assertEquals(1 , controlPokemonDTO.getActualHorizontalPosition());
        Assertions.assertEquals(0, controlPokemonDTO.getActualVerticalPosition());
        Assertions.assertEquals(POKEMON_NUMBER_EACH_SPOT * 2, controlPokemonDTO.getNumberPokemonFounded());
        Assertions.assertEquals(2, controlPokemonDTO.getHistoryPokemonPositionMap().size());

    }


    /**
     * If updateNumberAndHistoryPosition validates that is a new position
     * Should return the object with
     *      1- numberOfPokemon + 1
     *      2- historyPokemonPositionMap + new position
     */
    @Test
    void should_Update_Pokemon_And_HistoryMap_If_New_Horizontal_And_Vertical_Position() {

        //Create and Update ControlPokemonDTO
        ControlPokemonDTO controlPokemonDTO = new ControlPokemonDTO();
        controlPokemonDTO.setActualVerticalPosition(controlPokemonDTO.getActualVerticalPosition() + 1);
        controlPokemonDTO.setActualHorizontalPosition(controlPokemonDTO.getActualHorizontalPosition() + 1);

        controlPokemonDTO = PokemonFunctions.updateNumberAndHistoryPosition.apply(controlPokemonDTO);

        Assertions.assertEquals(1 , controlPokemonDTO.getActualHorizontalPosition());
        Assertions.assertEquals(1, controlPokemonDTO.getActualVerticalPosition());
        Assertions.assertEquals(POKEMON_NUMBER_EACH_SPOT * 2, controlPokemonDTO.getNumberPokemonFounded());
        Assertions.assertEquals(2, controlPokemonDTO.getHistoryPokemonPositionMap().size());

    }


    @Test
    void should_Normalize_Directions_String_Keeping_Only_E_S_O_W() {

        Assertions.assertEquals("", PokemonFunctions.normalizeDirectionString.apply(null));

        Assertions.assertEquals("", PokemonFunctions.normalizeDirectionString.apply("         "));

        Assertions.assertEquals("", PokemonFunctions.normalizeDirectionString.apply("ABCDFGHIJLMPQRTUVWXZY-.;´´@1234567890"));

        Assertions.assertEquals("SNOE", PokemonFunctions.normalizeDirectionString.apply("ABCDFGHIJLMSNOEPQRTUVWXZY-.;´´@1234567890"));

        Assertions.assertEquals("SNOE", PokemonFunctions.normalizeDirectionString.apply("SNOE"));

    }


    @Test
    void should_Concatenated_Vertical_And_Horizontal_Array() {

        Assertions.assertEquals("00" , PokemonFunctions.concatenateHorAndVerBiDimensionalArray.apply(new ControlPokemonDTO()));

    }


    }
