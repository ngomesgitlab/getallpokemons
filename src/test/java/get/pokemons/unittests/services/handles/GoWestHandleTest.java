package get.pokemons.unittests.services.handles;

import get.pokemons.PokemonApplication;
import get.pokemons.data.ControlPokemonDTO;
import get.pokemons.services.handles.GoWestHandle;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static get.pokemons.common.constants.PokemonConstants.EAST;
import static get.pokemons.common.constants.PokemonConstants.NORTH;
import static get.pokemons.common.constants.PokemonConstants.SOUTH;
import static get.pokemons.common.constants.PokemonConstants.WEST;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = {PokemonApplication.class})
@ContextConfiguration
class GoWestHandleTest {

    @Autowired
    GoWestHandle goWestHandle;

    @Test
    void should_Return_True_If_Is_West_Direction() {

        Assertions.assertTrue(goWestHandle.canHandle(WEST));

    }

    @Test
    void should_Return_False_If_Is_Other_Direction() {

        Assertions.assertFalse(goWestHandle.canHandle(SOUTH));
        Assertions.assertFalse(goWestHandle.canHandle(EAST));
        Assertions.assertFalse(goWestHandle.canHandle(NORTH));

    }

    @Test
    void should_Update_Actual_Horizontal_Direction_Minus_1() {

        ControlPokemonDTO pokemonNewPosition = goWestHandle.getPokemonNewPosition(new ControlPokemonDTO());
        Assertions.assertEquals(-1, pokemonNewPosition.getActualHorizontalPosition());
        Assertions.assertEquals(0, pokemonNewPosition.getActualVerticalPosition());

    }

}
