package get.pokemons.unittests.services.handles;

import get.pokemons.PokemonApplication;
import get.pokemons.data.ControlPokemonDTO;
import get.pokemons.services.handles.GoSouthHandle;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static get.pokemons.common.constants.PokemonConstants.EAST;
import static get.pokemons.common.constants.PokemonConstants.NORTH;
import static get.pokemons.common.constants.PokemonConstants.SOUTH;
import static get.pokemons.common.constants.PokemonConstants.WEST;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = {PokemonApplication.class})
@ContextConfiguration
class GoSouthHandleTest {

    @Autowired
    GoSouthHandle goSouthHandle;

    @Test
    void should_Return_True_If_Is_South_Direction() {

        Assertions.assertTrue(goSouthHandle.canHandle(SOUTH));

    }

    @Test
    void should_Return_False_If_Is_Other_Direction() {

        Assertions.assertFalse(goSouthHandle.canHandle(WEST));
        Assertions.assertFalse(goSouthHandle.canHandle(EAST));
        Assertions.assertFalse(goSouthHandle.canHandle(NORTH));

    }

    @Test
    void should_Update_Actual_Vertical_Direction_Minus_1() {

        ControlPokemonDTO pokemonNewPosition = goSouthHandle.getPokemonNewPosition(new ControlPokemonDTO());
        Assertions.assertEquals(0, pokemonNewPosition.getActualHorizontalPosition());
        Assertions.assertEquals(-1, pokemonNewPosition.getActualVerticalPosition());

    }

}
