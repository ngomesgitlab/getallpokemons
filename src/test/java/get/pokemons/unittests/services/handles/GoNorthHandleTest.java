package get.pokemons.unittests.services.handles;

import get.pokemons.PokemonApplication;
import get.pokemons.data.ControlPokemonDTO;
import get.pokemons.services.handles.GoNorthHandle;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static get.pokemons.common.constants.PokemonConstants.EAST;
import static get.pokemons.common.constants.PokemonConstants.NORTH;
import static get.pokemons.common.constants.PokemonConstants.SOUTH;
import static get.pokemons.common.constants.PokemonConstants.WEST;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = {PokemonApplication.class})
@ContextConfiguration
class GoNorthHandleTest {

    @Autowired
    GoNorthHandle goNorthHandle;

    @Test
    void should_Return_True_If_Is_North_Direction() {

        Assertions.assertTrue(goNorthHandle.canHandle(NORTH));

    }

    @Test
    void should_Return_False_If_Is_Other_Direction() {

        Assertions.assertFalse(goNorthHandle.canHandle(WEST));
        Assertions.assertFalse(goNorthHandle.canHandle(EAST));
        Assertions.assertFalse(goNorthHandle.canHandle(SOUTH));

    }

    @Test
    void should_Update_Actual_Vertical_Direction_Plus_1() {

        ControlPokemonDTO pokemonNewPosition = goNorthHandle.getPokemonNewPosition(new ControlPokemonDTO());
        Assertions.assertEquals(0, pokemonNewPosition.getActualHorizontalPosition());
        Assertions.assertEquals(1, pokemonNewPosition.getActualVerticalPosition());

    }

}
