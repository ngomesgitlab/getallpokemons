package get.pokemons.unittests.services;

import get.pokemons.PokemonApplication;
import get.pokemons.common.predicates.GenericPredicates;
import get.pokemons.services.GetPokemonControlHandles;
import get.pokemons.services.manager.GetPokemonValidationHandlesMap;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = {PokemonApplication.class})
@ContextConfiguration
class GetPokemonControlHandlesTest {

    @Autowired
    private GetPokemonControlHandles getPokemonControlHandles;

    @Test
    void should_Get_Not_Null_Not_Empty_Pokemon_Control_Handles_Map() {

        GetPokemonValidationHandlesMap getPokemonValidationHandlesMap = getPokemonControlHandles.getGetPokemonValidationHandlesMap();
        Assertions.assertFalse(GenericPredicates.checkIfNullOrEmpty.test(getPokemonValidationHandlesMap));

    }

}
