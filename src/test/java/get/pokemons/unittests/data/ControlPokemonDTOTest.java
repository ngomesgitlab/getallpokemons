package get.pokemons.unittests.data;

import get.pokemons.PokemonApplication;
import get.pokemons.common.predicates.GenericPredicates;
import get.pokemons.data.ControlPokemonDTO;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Map;

import static get.pokemons.common.constants.PokemonConstants.POKEMON_NUMBER_EACH_SPOT;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = {PokemonApplication.class})
@ContextConfiguration
class ControlPokemonDTOTest {

    @Test
    void should_Get_Valid_Default_Control_Pokemon_DTO() {

        ControlPokemonDTO controlPokemonDTO = new ControlPokemonDTO();

        //validate object
        Assertions.assertFalse(GenericPredicates.checkIfNullOrEmpty.test(controlPokemonDTO));

        //validate numberPokemonFounded
        Assertions.assertEquals(POKEMON_NUMBER_EACH_SPOT, controlPokemonDTO.getNumberPokemonFounded());

        //validate actualHorizontalPosition(
        Assertions.assertEquals(0, controlPokemonDTO.getActualHorizontalPosition());

        //validate actualVerticalPosition
        Assertions.assertEquals(0, controlPokemonDTO.getActualVerticalPosition());

        //validate historyPokemonPositionMap - Should contain 1 entry with Key 0 , and Set - Size 1 and With 0 as an entry
        Map<String, Boolean> historyPokemonPositionMap = controlPokemonDTO.getHistoryPokemonPositionMap();
        Assertions.assertFalse(GenericPredicates.checkIfNullOrEmpty.test(historyPokemonPositionMap));
        Assertions.assertEquals(1, historyPokemonPositionMap.size());
        Assertions.assertTrue(historyPokemonPositionMap.get("00"));

    }

}
