package get.pokemons.integrationtests;

import get.pokemons.PokemonApplication;
import get.pokemons.services.GetPokemonControlProcess;
import get.pokemons.services.GetPokemonServices;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Spy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = {PokemonApplication.class})
@ContextConfiguration
class GetPokemonServicesExceptionTest {

    @Autowired
    GetPokemonControlProcess getPokemonControlProcess;

    @Spy
    GetPokemonServices getPokemonServices = new GetPokemonServices(getPokemonControlProcess);

    @Test
    void should_Thrown_Exception_And_Return_Zero_Pokemons_Founded() {

        when(getPokemonServices.getPokemonServices("NENE")).thenThrow(new NullPointerException());

        Assertions.assertThrows(NullPointerException.class, () -> {
            int numPokemon = getPokemonServices.getPokemonServices("NENE");
            Assertions.assertEquals(0, numPokemon);
        });

    }




}
