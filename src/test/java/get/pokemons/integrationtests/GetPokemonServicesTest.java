package get.pokemons.integrationtests;

import get.pokemons.PokemonApplication;
import get.pokemons.services.GetPokemonServices;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static get.pokemons.common.constants.PokemonConstants.POKEMON_NUMBER_EACH_SPOT;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = {PokemonApplication.class})
@ContextConfiguration
class GetPokemonServicesTest {

    @Autowired
    GetPokemonServices getPokemonServices;


    @Test
    void should_Catch_Pokemon_Directions_Null() {

        /**
         * POKEMON_NUMBER_EACH_SPOT * Number of new spots visited (1)
         */
        Assertions.assertEquals(POKEMON_NUMBER_EACH_SPOT, getPokemonServices.getPokemonServices(null));

    }


    @Test
    void should_Catch_Pokemon_Directions_Empty() {

        /**
         * POKEMON_NUMBER_EACH_SPOT * Number of new spots visited (1)
         */
        Assertions.assertEquals(POKEMON_NUMBER_EACH_SPOT, getPokemonServices.getPokemonServices("         "));

    }


    @Test
    void should_Catch_Pokemon_Directions_NESO() {

        /**
         * POKEMON_NUMBER_EACH_SPOT * Number of new spots visited (4)
         */
        Assertions.assertEquals(POKEMON_NUMBER_EACH_SPOT * 4, getPokemonServices.getPokemonServices("NESO"));

    }


    @Test
    void should_Catch_Pokemon_Directions_NESONESONESONESOE() {

        /**
         * POKEMON_NUMBER_EACH_SPOT * Number of new spots visited (4)
         */
        Assertions.assertEquals(POKEMON_NUMBER_EACH_SPOT * 4, getPokemonServices.getPokemonServices("NESONESONESONESONESONESO"));

    }


    @Test
    void should_Catch_Pokemon_Directions_NSNSNSNSNS() {

        /**
         * POKEMON_NUMBER_EACH_SPOT * Number of new spots visited (2)
         */
        Assertions.assertEquals(POKEMON_NUMBER_EACH_SPOT * 2, getPokemonServices.getPokemonServices("NSNSNSNSNS"));

    }


    @Test
    void should_Catch_Pokemon_Directions_ABCDFGHIJLMOPQRTUVXZY() {

        /**
         * POKEMON_NUMBER_EACH_SPOT * Number of new spots visited (1)
         */
        Assertions.assertEquals(POKEMON_NUMBER_EACH_SPOT, getPokemonServices.getPokemonServices("ABCDFGHIJLMPQRTUVWXZY"));

    }


    @Test
    void should_Catch_Pokemon_Directions_NESONESONESONESOENN() {

        /**
         * POKEMON_NUMBER_EACH_SPOT * Number of new spots visited (9)
         */
        Assertions.assertEquals(POKEMON_NUMBER_EACH_SPOT * 9, getPokemonServices.getPokemonServices("NESONESONESONESONESONESONNNESONESONESONESONNS"));

    }

}
