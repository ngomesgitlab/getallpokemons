## Title

  Pokemon: apanhá-los todos

## Technical Environment Rules

    1 - Podes usar a linguagem / stack tecnológica que preferires. No entanto, é preciso que, com o teu
    código-fonte, nos forneças o comando completo que permite compilar (se necessário) e executar
    a tua solução.
    2 - A tua solução deve ser correta (do ponto de vista funcional) e eficiente (tempo gasto, memória
    ocupada, etc.). Sugerimos portanto que escrevas testes que ponham à prova a tua solução para lá
    dos inputs de exemplo (inputs de maior dimensão, casos bicudos, etc.), e que os incluas no
    código-fonte que enviares.
    3 - Também damos (muito) valor ao quão compreensível e organizado é o teu código.

## Goal

    O Ash está a apanhar pokémons num mundo que consiste numa grelha bidimensional infinita de casas.
    Em cada casa há exatamente um pokémon.
    O Ash começa por apanhar o pokémon que está na casa onde começa. A seguir, move-se para a casa
    imediatamente a norte, sul, este ou oeste de onde se encontra e apanha o pokémon que aí se encontrar,
    e assim sucessivamente. Atenção: se ele passar numa casa onde já passou (e, portanto, onde já apanhou
    um pokémon), já lá não está um pokémon para ele apanhar!
    O que queremos saber é: começando com um mundo cheio de pokémons (um em cada casa!), quantos
    pokémons o Ash apanha para uma dada sequência de movimentos?
    Formato do input
    O programa deve ler uma linha do stdin, que contém uma sequência de movimentos. Cada movimento é
    descrito por uma letra N, S, Eou O(respetivamente: norte, sul, este, oeste).
    Formato do output
    O programa deve escrever uma linha para o stdout, apenas com um número: quantos pokémons o Ash
    apanhou?

    Exemplos :

    1 - Input  : E
        OutPut : 2

    2 - Input  : NESO
        OutPut : 4

    3 - Input  : NSNSNSNSNS
        OutPut : 2

    Writing tests!

## Tooling

  1. Please install IntelliJ or Eclipse Lombok plugin, to avoid possible compilation problems;
  2. Java 11;
  3. Maven;

# First Steps

  1. You can start the application running : mvn spring-boot:run -Dspring-boot.run.arguments="startApplication"

## Tech Stack

  1. Spring Framework

      - Spring Boot, Sprint Test
      - Please check POM for more information

  2. Lombok Plugin :

      - Reduce Boiler Plate code

  3. CheckStyle :

      - Code style pattern plugin (checkstyle.xml)

## Tests

  1. Unit and Integration Tests :

    - Folder : Src -> Test -> Java -> pokemons -> unittests

  2. Integration Tests :

    - Folder : Src -> Test -> Java -> pokemons -> integrationtests

  3. Tests Support Classes :

    - Folder : Src -> Test -> Java -> pokemons -> testsupport


## GIT Lab

  If you want to login in to validate the project in GITLab you can use this login / password :

    1 - User : ngomesgitlab
    2 - Password : ngomesgitlab@1234

## Design Patterns

  1. I like to use Chain of Responsibility Design Pattern during validation process to de-couple one validation from another and to avoid create a LOT of if´s.
